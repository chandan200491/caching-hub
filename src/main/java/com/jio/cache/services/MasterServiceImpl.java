package com.jio.cache.services;

import com.jio.cache.adapter.CacheDataAdapter;
import com.jio.cache.dao.MasterDAO;
import com.jio.cache.dto.CacheResponseDTO;
import com.jio.cache.entity.Bin;
import com.jio.cache.entity.MasterConfigBean;
import com.jio.cache.entity.MasterDataBean;
import com.jio.cache.exceptions.BaseApplicationException;
import com.jio.cache.exceptions.BusinessException;
import com.jio.cache.exceptions.DaoException;
import com.jio.cache.util.CacheConstant;
import com.jio.cache.util.InputConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MasterServiceImpl implements MasterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MasterServiceImpl.class);
    @Autowired
    private RedisServices redisServices;
    @Autowired
    private MasterDAO masterDAO;
    @Autowired
    private IJSONService ijsonService;
    @Autowired
    private CacheDataAdapter cacheDataAdapter;

    /**
     * @param key
     * @return
     * @throws BusinessException
     */
    public <T> T getMasterConfig(String key) throws BaseApplicationException {
        Object keyBean = redisServices.getValue(key);
        if (keyBean == null) {
            LOGGER.info("key {} not found in cache. Loading from database.", key);
            MasterConfigBean masterConfig = null;
            masterConfig = masterDAO.getMasterConfig(key);
            if (masterConfig == null) {
                return (T) "";
            }
            redisServices.setValue(key, masterConfig.getValue());
            return (T) masterConfig.getValue();
        }
        return (T) keyBean;
    }

    /**
     * @param inputMap
     * @return
     * @throws BaseApplicationException
     */
    public CacheResponseDTO getMasterData(Map<InputConstant, String> inputMap) throws BaseApplicationException {
        boolean isBinPresent = checkForBinOperation(inputMap);
        if (isBinPresent) {
            return getBinData(inputMap);
        } else {
            return getMasterInputData(inputMap);
        }
    }

    /**
     * @return
     * @throws BusinessException
     */
    private CacheResponseDTO getBinData(Map<InputConstant, String> inputMap) throws BaseApplicationException {
        CacheResponseDTO cacheResponseDTO = fetchBinDataFromCache(inputMap);
        if (cacheResponseDTO != null) {
            return cacheResponseDTO;
        }
        List<Bin> bins = masterDAO.getBinFromDb(inputMap.get(InputConstant.CARD_NO), inputMap.get(InputConstant.PAN_LENGTH));
        if (CollectionUtils.isEmpty(bins)) {
            throw new BusinessException(CacheConstant.ERROR_CODE, "No Bin detail found for given input parameter");
        }
        LOGGER.info("Going to transform Bin data for input parameter: {}", inputMap);
        cacheResponseDTO = cacheDataAdapter.tranformBinData(bins, inputMap.get(InputConstant.CARD_NO));
        saveBinDataInCacheServer(inputMap.get(InputConstant.APP_CODE), cacheResponseDTO);
        return cacheResponseDTO;
    }

    /**
     * @param inputMap
     * @return
     * @throws BaseApplicationException
     */
    private CacheResponseDTO getMasterInputData(Map<InputConstant, String>  inputMap) throws BaseApplicationException {
        CacheResponseDTO cacheResponseDTO = fetchDataFromCache(inputMap);
        if (cacheResponseDTO != null) {
            return cacheResponseDTO;
        }
        List<MasterDataBean> masterDataBeans = masterDAO.getMasterDataFromDb(inputMap);
        if (CollectionUtils.isEmpty(masterDataBeans)) {
            throw new BusinessException(CacheConstant.ERROR_CODE, "No data found for given input parameter");
        }
        LOGGER.info("Going to transform master data for input parameter: {}", inputMap);
        cacheResponseDTO = cacheDataAdapter.tranformMasterData(masterDataBeans);
        saveMasterDataInCacheServer(inputMap.get(InputConstant.APP_CODE), cacheResponseDTO);
        return cacheResponseDTO;
    }

    /**
     * @param inputMap
     * @return
     * @throws BusinessException
     */
    private CacheResponseDTO fetchDataFromCache(Map<InputConstant, String> inputMap) throws BusinessException {
        LOGGER.info("Fetching data from cache server for given parameter:{}", inputMap);
        try {
            Object value = redisServices.getValue(inputMap.get(InputConstant.APP_CODE));
            if (value == null) {
                LOGGER.info("Data is not present in cache server for given parameter: {}", inputMap);
                return null;
            }
            Map<String, String> masterDataMap = (Map<String, String>) value;
            if (!masterDataMap.containsKey(inputMap.get(InputConstant.OPERATION_CODE))) {
                LOGGER.info("Data is not present in redis server for given operation code: {}", inputMap);
                return null;
            }
            String masterJsonData = masterDataMap.get(inputMap.get(InputConstant.OPERATION_CODE));
            Map<String, List<Map<String, String>>> dataMap = ijsonService.getObjectFromJson(masterJsonData, Map.class);
            CacheResponseDTO cacheResponseDTO = new CacheResponseDTO();
            Map<String, Map<String, List<Map<String, String>>>> output = new HashMap<>();
            output.put(inputMap.get(InputConstant.OPERATION_CODE), dataMap);
            cacheResponseDTO.setOutput(output);
            return cacheResponseDTO;
        } catch (Exception e) {
            LOGGER.error("Exception while fetching cached data from redis for input parameter: {}", inputMap, e);
        }
        return null;
    }

    /**
     * @param inputMap
     * @return
     * @throws BusinessException
     */
    private CacheResponseDTO fetchBinDataFromCache(Map<InputConstant, String> inputMap) throws BusinessException {
        LOGGER.info("Fetching data from cache server for given parameter:{}", inputMap);
        try {
            Object value = redisServices.getValue(inputMap.get(InputConstant.APP_CODE));
            if (value == null) {
                LOGGER.info("Data is not present in cache server for given parameter: {}", inputMap);
                return null;
            }
            Map<String, String> masterDataMap = (Map<String, String>) value;
            if (!masterDataMap.containsKey(inputMap.get(InputConstant.OPERATION_CODE))) {
                LOGGER.info("Data is not present in cache server for given operation code: {}", inputMap);
                return null;
            }
            String masterJsonData = masterDataMap.get(inputMap.get(InputConstant.OPERATION_CODE));
            Map<String, List<Map<String, String>>> dataMap = ijsonService.getObjectFromJson(masterJsonData, Map.class);
            Map<String, Map<String, List<Map<String, String>>>> output = new HashMap<>();
            Map<String, List<Map<String, String>>> cardDataMap = null;
            if (dataMap != null) {
                if (dataMap.containsKey(inputMap.get(InputConstant.CARD_NO))) {
                    cardDataMap = new HashMap<>();
                    cardDataMap.put(inputMap.get(InputConstant.CARD_NO), dataMap.get(inputMap.get(InputConstant.CARD_NO)));
                    output.put(inputMap.get(InputConstant.OPERATION_CODE), cardDataMap);
                    CacheResponseDTO cacheResponseDTO = new CacheResponseDTO();
                    cacheResponseDTO.setOutput(output);
                    return cacheResponseDTO;
                }
            }
            LOGGER.info("Data is not present in cache server for given operation code: {}", inputMap);
        } catch (Exception e) {
            LOGGER.error("Exception while fetching Bin cached data from redis for input parameter: {}", inputMap, e);
        }
        return null;
    }

    /**
     * @param appCode
     * @param cacheResponseDTO
     */
    private void saveMasterDataInCacheServer(String appCode, CacheResponseDTO cacheResponseDTO) {
        LOGGER.info("Saving master data in cache server for appCode :{}", appCode);
        try {
            Object val = redisServices.getValue(appCode);
            Map<String, String> dataMap = null;
            if (val == null) {
                dataMap = new HashMap<>();
            } else {
                dataMap = (Map<String, String>) val;
            }
            Map<String, Map<String, List<Map<String, String>>>> output = cacheResponseDTO.getOutput();
            Map<String, List<Map<String, String>>> dataValueMap = null;
            String jsonValue = null;
            for (String operationCode : output.keySet()) {
                if (dataMap != null && dataMap.containsKey(operationCode)) {
                    dataValueMap = ijsonService.getObjectFromJson(dataMap.get(operationCode), Map.class);
                    dataValueMap.putAll(output.get(operationCode));
                } else {
                    dataValueMap = output.get(operationCode);
                }
                jsonValue = ijsonService.getJSONString(dataValueMap);
                dataMap.put(operationCode, jsonValue);
            }
            redisServices.setValue(appCode, dataMap);
        } catch (Exception e) {
            LOGGER.error("Exception while saving master data in cache server :{}", appCode, e);
        }
    }

    /**
     * @param appCode
     * @param cacheResponseDTO
     */
    private void saveBinDataInCacheServer(String appCode, CacheResponseDTO cacheResponseDTO) {
        LOGGER.info("Saving Bin data in cache server for appCode :{}", appCode);
        try {
            Object val = redisServices.getValue(appCode);
            Map<String, String> dataMap = null;
            if (val == null) {
                dataMap = new HashMap<>();
            } else {
                dataMap = (Map<String, String>) val;
            }
            Map<String, Map<String, List<Map<String, String>>>> output = cacheResponseDTO.getOutput();
            Map<String, List<Map<String, String>>> dataValueMap = null;
            String jsonValue = null;

            for (String operationCode : output.keySet()) {
                if (dataMap != null && dataMap.containsKey(operationCode)) {
                    dataValueMap = ijsonService.getObjectFromJson(dataMap.get(operationCode), Map.class);
                    dataValueMap.putAll(output.get(operationCode));
                } else {
                    dataValueMap = output.get(operationCode);
                }
                jsonValue = ijsonService.getJSONString(dataValueMap);
                dataMap.put(operationCode, jsonValue);
            }
            redisServices.setValue(appCode, dataMap);
        } catch (Exception e) {
            LOGGER.error("Exception while saving Bin Master data in cache server :{}", appCode, e);
        }
    }

    /**
     * @param inputMap
     * @return
     * @throws BusinessException
     */
    private boolean checkForBinOperation(Map<InputConstant, String> inputMap) throws BusinessException {
        if (inputMap.containsKey(InputConstant.OPERATION_CODE) && "BIN".equalsIgnoreCase(inputMap.get(InputConstant.OPERATION_CODE))) {
            if (!inputMap.containsKey(InputConstant.CARD_NO)) {
                throw new BusinessException(CacheConstant.ERROR_CODE, "Kindly provide card number.");
            } else if (inputMap.get(InputConstant.CARD_NO).length()>16) {
                throw new BusinessException(CacheConstant.ERROR_CODE, "Card number length should not be greater than 16.");
            } else if (!inputMap.containsKey(InputConstant.PAN_LENGTH)) {
                throw new BusinessException(CacheConstant.ERROR_CODE, "Kindly provide pan length");
            }
            return true;
        }
        return false;
    }

}
