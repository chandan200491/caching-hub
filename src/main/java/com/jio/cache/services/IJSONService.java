package com.jio.cache.services;

public interface IJSONService {

	<T> String getJSONString(T objectToBeTransformed);

	<E> E getObjectFromJson(String json, Class<E> classType);

}
