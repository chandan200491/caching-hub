package com.jio.cache.services;

import java.util.concurrent.TimeUnit;

/**
 *
 */

public interface RedisServices {

    Object getValue(final String key);
	
    void setValue(final String key, String value);
	
    void setValue(final String key, Object value);
	
    void setValue(final String key, final String value, long time, TimeUnit timeUnit);
    
    void setValue(final String key, Object value, long time, TimeUnit timeUnit);
	
    void deleteKey(final String key);

    Object saveAtomic(String key, Object value, String cacheExpiry, TimeUnit timeUnit);
}
