package com.jio.cache.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisServicesImpl implements RedisServices {

    @Autowired
    private RedisTemplate<String, Object> template;

    @Override
    public Object getValue(final String key) {
        return template.opsForValue().get(key);
    }

    @Override
    public void setValue(final String key, final String value) {
        template.opsForValue().set(key, value);
    }

    @Override
    public void setValue(final String key, final String value, long time, TimeUnit timeUnit) {
        template.opsForValue().set(key, value);
        template.expire(key, time, timeUnit);
    }

    @Override
    public void setValue(final String key, Object value, long time, TimeUnit timeUnit) {
        template.opsForValue().set(key, value);
        template.expire(key, time, timeUnit);
    }

    @Override
    public void deleteKey(String key) {
        template.delete(key);
    }

    @Override
    public void setValue(String key, Object value) {
        template.opsForValue().set(key, value);
    }

    /**
     * @param key
     * @param value
     * @param cacheExpiry
     * @param timeUnit
     * @return
     */
    public Object saveAtomic(String key, Object value, String cacheExpiry, TimeUnit timeUnit) {
        Object oldvalue = template.opsForValue().getAndSet(key, value);
        template.expire(key, Integer.parseInt(cacheExpiry.trim()), timeUnit);
        return oldvalue;
    }
}
