package com.jio.cache.services;

import com.jio.cache.dto.CacheResponseDTO;
import com.jio.cache.exceptions.BaseApplicationException;
import com.jio.cache.exceptions.BusinessException;
import com.jio.cache.util.InputConstant;

import java.util.List;
import java.util.Map;

/**
 * @author Chandan Singh Karki
 */
public interface MasterService {
    public <T> T  getMasterConfig(String key) throws BaseApplicationException;
    public CacheResponseDTO getMasterData(Map<InputConstant,String> inputMap) throws BaseApplicationException;

}
