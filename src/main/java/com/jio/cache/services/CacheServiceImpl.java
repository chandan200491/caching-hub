package com.jio.cache.services;

import com.jio.cache.dto.CacheClearDTO;
import com.jio.cache.dto.CacheInputDTO;
import com.jio.cache.dto.CacheResponseDTO;
import com.jio.cache.dto.StatusBean;
import com.jio.cache.exceptions.BaseApplicationException;
import com.jio.cache.exceptions.BusinessException;
import com.jio.cache.util.CacheClearInputConstant;
import com.jio.cache.util.CacheConstant;
import com.jio.cache.util.InputConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service
public class CacheServiceImpl implements CacheService {

    @Autowired
    private MasterService masterService;
    @Autowired
    private RedisServices redisServices;
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheServiceImpl.class);


    /**
     * @param cacheInputDTO
     * @return cacheResponseDTO
     * @throws BusinessException
     */
    public CacheResponseDTO getMasterData(CacheInputDTO cacheInputDTO) throws BaseApplicationException {
        validateInputCacheParameter(cacheInputDTO);
        CacheResponseDTO cacheResponseDTO = masterService.getMasterData(cacheInputDTO.getInputMap());
        return cacheResponseDTO;
    }

    /**
     * @param cacheClearDTO
     * @throws BusinessException
     */
    public void clearCachedData(CacheClearDTO cacheClearDTO) throws BusinessException {
        validateCacheClearInputParameter(cacheClearDTO);
        clearCacheData(cacheClearDTO);
    }

    /**
     * @param cacheInputDTO
     * @throws BusinessException
     */
    private void validateInputCacheParameter(CacheInputDTO cacheInputDTO) throws BusinessException {
        if (cacheInputDTO == null || CollectionUtils.isEmpty(cacheInputDTO.getInputMap())) {
            throw new BusinessException(CacheConstant.ERROR_CODE, "kindly provide some input parameter");
        }
        if (!cacheInputDTO.getInputMap().containsKey(InputConstant.APP_CODE)) {
            throw new BusinessException(CacheConstant.ERROR_CODE, "Kindly provide application code");
        }
    }

    /**
     * @param cacheClearDTO
     * @throws BusinessException
     */
    private void validateCacheClearInputParameter(CacheClearDTO cacheClearDTO) throws BusinessException {
        if (cacheClearDTO == null || CollectionUtils.isEmpty(cacheClearDTO.getCacheClearParam())) {
            throw new BusinessException(CacheConstant.ERROR_CODE, "kindly provide some input parameter");
        }
        Map<CacheClearInputConstant, String> cacheInputParmeter = cacheClearDTO.getCacheClearParam();
        if (!cacheInputParmeter.containsKey(CacheClearInputConstant.APP_CODE)) {
            throw new BusinessException(CacheConstant.ERROR_CODE, "Kindly provide application code as a input parameter");
        }
    }

    /**
     * @param cacheClearDTO
     * @throws BusinessException
     */
    private void clearCacheData(CacheClearDTO cacheClearDTO) throws BusinessException {
        String appCode = null;
        String operationCode = null;
        Map<CacheClearInputConstant, String> cacheInputParmeter = cacheClearDTO.getCacheClearParam();
        for (CacheClearInputConstant key : cacheInputParmeter.keySet()) {
            switch (key) {
                case APP_CODE:
                    appCode = cacheInputParmeter.get(key);
                    break;
                case OPERATION_CODE:
                    operationCode = cacheInputParmeter.get(key);
                    break;
            }
        }
        if (!StringUtils.isEmpty(appCode)) {
            Object value = redisServices.getValue(appCode);

            if (value == null) {
                throw new BusinessException(CacheConstant.ERROR_CODE, "No data found in cache server for given application code");
            }

            if (StringUtils.isEmpty(operationCode)) {
                redisServices.deleteKey(appCode);
            } else {
                if (value != null) {
                    Map<String, String> operationCodeMap = (Map<String, String>) value;
                    if (operationCodeMap.containsKey(operationCode)) {
                        operationCodeMap.remove(operationCode);
                        redisServices.setValue(appCode, operationCodeMap);
                        LOGGER.info("Data cleared from Cache server successfully");
                        return;
                    }
                }
                throw new BusinessException(CacheConstant.ERROR_CODE, "No data found in cache server for given operation code");
            }
        }
    }


}
