package com.jio.cache.services;

import com.google.gson.Gson;
import org.springframework.stereotype.Service;

/**
 * This class is used to convert json to object and vice versa. Every class
 * performing any conversion must use this service.
 */
@Service
public class JSONServiceImpl implements IJSONService {

    private Gson gson = new Gson();

    @Override
    public <T> String getJSONString(T objectToBeTransformed) {
        String jsonString = gson.toJson(objectToBeTransformed);
        return jsonString;
    }

    @Override
    public <E> E getObjectFromJson(String json, Class<E> classType) {
        E jsonObject = gson.fromJson(json, classType);
        return jsonObject;
    }
}
