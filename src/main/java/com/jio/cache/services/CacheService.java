package com.jio.cache.services;

import com.jio.cache.dto.CacheClearDTO;
import com.jio.cache.dto.CacheInputDTO;
import com.jio.cache.dto.CacheResponseDTO;
import com.jio.cache.dto.StatusBean;
import com.jio.cache.exceptions.BaseApplicationException;
import com.jio.cache.exceptions.BusinessException;

import java.util.List;

public interface CacheService {
    public CacheResponseDTO getMasterData(CacheInputDTO cacheInputDTO) throws BaseApplicationException;
    public void clearCachedData(CacheClearDTO cacheClearDTO) throws BusinessException;
}
