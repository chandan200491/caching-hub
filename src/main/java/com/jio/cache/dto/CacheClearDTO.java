package com.jio.cache.dto;

import com.jio.cache.util.CacheClearInputConstant;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CacheClearDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<CacheClearInputConstant, String> cacheClearParam;

    public Map<CacheClearInputConstant, String> getCacheClearParam() {
        return cacheClearParam;
    }

    public void setCacheClearParam(Map<CacheClearInputConstant, String> cacheClearParam) {
        this.cacheClearParam = cacheClearParam;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("cacheClearParam", cacheClearParam)
                .toString();
    }
}
