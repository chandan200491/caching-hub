package com.jio.cache.dto;

import com.jio.cache.util.InputConstant;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Map;

public class CacheInputDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Map<InputConstant,String> inputMap;

    public Map<InputConstant, String> getInputMap() {
        return inputMap;
    }

    public void setInputMap(Map<InputConstant, String> inputMap) {
        this.inputMap = inputMap;
    }

    public CacheInputDTO(Map<InputConstant, String> inputMap) {
        this.inputMap = inputMap;
    }

    public CacheInputDTO() {
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("inputMap", inputMap)
                .toString();
    }
}
