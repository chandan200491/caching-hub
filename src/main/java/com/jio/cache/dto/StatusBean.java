package com.jio.cache.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * @author Chandan Singh karki
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatusBean implements Serializable {
    
	private static final long serialVersionUID = 4097634263865366524L;
	/** The status code. */
	private String statusCode;
	
	/** The status message. */
    private String statusMessage;

    private Object result;



    public StatusBean() {
    }

	public StatusBean(String statusCode, String statusMessage) {
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
	}

	public StatusBean(String statusCode, String statusMessage, Object result) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.result=result;
    }

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
}

