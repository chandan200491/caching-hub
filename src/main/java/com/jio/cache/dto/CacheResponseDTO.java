package com.jio.cache.dto;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CacheResponseDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Map<String, Map<String, List<Map<String, String>>>> output;

    public CacheResponseDTO() {
    }

    public Map<String, Map<String, List<Map<String, String>>>> getOutput() {
        return output;
    }

    public void setOutput(Map<String, Map<String, List<Map<String, String>>>> output) {
        this.output = output;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("output", output)
                .toString();
    }
}
