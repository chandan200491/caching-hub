package com.jio.cache.util;

/**
 * @author Chandan Singh Karki
 * */

public interface MasterConfigKeys {
    String REDIS_IP = "redis.ip";
    String REDIS_PORT = "redis.port";
    String REDIS_POOL_MAX_TOTAL = "redis.pool.max.total";
    String REDIS_POOL_MAX_IDLE = "redis.pool.max.idle";
    String REDIS_POOL_MIN_IDLE = "redis.pool.min.idle";
    String REDIS_SENTINEL_MASTER = "redis.sentinel.master";
    String REDIS_SENTINEL_HOST_PORTS = "redis.sentinel.host.and.ports";
}
