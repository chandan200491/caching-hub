package com.jio.cache.util;

public interface CacheConstant {
    String DAO_ERROR="DAO_ERROR";
    String DAO_MESSAGE="ERROR WHILE DATABASE OPERATION";
    String ERROR_CODE="ERROR";
    String SUCCESS_CODE="SUCCESS";
    String BIN_OPERATION="BIN";
    String CARD_SCHEME="CARD_SCHEME";
    String ACCOUNT_SRC="ACCOUNT_SRC";
}
