package com.jio.cache.util;

public enum InputConstant {
    APP_CODE, OPERATION_CODE, LABEL, KEY, CARD_NO, PAN_LENGTH;

    InputConstant() {
    }
}
