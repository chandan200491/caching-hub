package com.jio.cache.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jio.cache.dao.MasterDAO;
import com.jio.cache.entity.MasterConfigBean;
import com.jio.cache.exceptions.DaoException;
import com.jio.cache.util.MasterConfigKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chandan Singh Karki
 */
@Configuration
public class RedisConfiguration {
    @Autowired
    private MasterDAO masterDAO;
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisConfiguration.class);

    @Bean
    public JedisPoolConfig poolConfig() throws DaoException {
        final JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setTestOnBorrow(true);
        MasterConfigBean master = masterDAO.getMasterConfig(MasterConfigKeys.REDIS_POOL_MAX_TOTAL);
        jedisPoolConfig.setMaxTotal(Integer.valueOf(master.getValue()));
        master = masterDAO.getMasterConfig(MasterConfigKeys.REDIS_POOL_MAX_IDLE);
        jedisPoolConfig.setMaxIdle(Integer.valueOf(master.getValue()));
        master = masterDAO.getMasterConfig(MasterConfigKeys.REDIS_POOL_MIN_IDLE);
        jedisPoolConfig.setMinIdle(Integer.valueOf(master.getValue()));
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setTestOnReturn(true);
        jedisPoolConfig.setTestWhileIdle(true);
        return jedisPoolConfig;
    }

    @Bean(name = "jedisConnectionFactory")
    JedisConnectionFactory jedisConnectionFactory() throws DaoException {
        JedisConnectionFactory jedisConFactory = new JedisConnectionFactory(poolConfig());
        MasterConfigBean master = masterDAO.getMasterConfig(MasterConfigKeys.REDIS_IP);
        jedisConFactory.setHostName(master.getValue());
        master = masterDAO.getMasterConfig(MasterConfigKeys.REDIS_PORT);
        jedisConFactory.setPort(Integer.valueOf(master.getValue()));
        jedisConFactory.setUsePool(true);
        return jedisConFactory;
    }

 /*   @Bean(name = "jedisConnectionFactory")
    public JedisConnectionFactory jedisConnectionFactoryProd() throws DaoException {

        RedisSentinelConfiguration redisSentinelConfiguration = new RedisSentinelConfiguration();
        MasterConfigBean master = masterDAO.getMasterConfig(MasterConfigKeys.REDIS_SENTINEL_MASTER);
        redisSentinelConfiguration.master(master.getValue());// propertyUtil.getRedisSentinelMaster());
        master = masterDAO.getMasterConfig(MasterConfigKeys.REDIS_SENTINEL_HOST_PORTS);
        String redisSentinelHostAndPorts = master.getValue();
        if (null != redisSentinelHostAndPorts) {
            HostAndPort hostAndPort = null;
            if (redisSentinelHostAndPorts.contains(";")) {
                for (String node : redisSentinelHostAndPorts.split(";")) {
                    if (null != node && node.contains(":")) {
                        hostAndPort = new HostAndPort(node);
                        LOGGER.debug(", Host:Port::" + hostAndPort.getHost() + ":" + hostAndPort.getPort());
                        redisSentinelConfiguration.sentinel(hostAndPort.getHost(),
                                Integer.valueOf(hostAndPort.getPort()));
                    }
                }
            } else {
                if (redisSentinelHostAndPorts.contains(":")) {
                    hostAndPort = new HostAndPort(redisSentinelHostAndPorts);
                    LOGGER.debug(", else Host:Port::" + hostAndPort.getHost() + ":" + hostAndPort.getPort());
                    redisSentinelConfiguration.sentinel(hostAndPort.getHost(), Integer.valueOf(hostAndPort.getPort()));
                }
            }
        } else {

            LOGGER.debug(", Host and Ports are not specified.");
        }
        return new JedisConnectionFactory(redisSentinelConfiguration, poolConfig());
    }*/


    @Bean
    public StringRedisSerializer stringRedisSerialized() {
        return new StringRedisSerializer();
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate(
            @Qualifier("jedisConnectionFactory") JedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setKeySerializer(stringRedisSerialized());
        template.setConnectionFactory(connectionFactory);
        return template;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        return objectMapper;
    }

    private class HostAndPort {

        private String host;
        private int port;
        private String[] hostAndPortStringArray = new String[2];

        HostAndPort(String hostAndPort) {

            hostAndPortStringArray = hostAndPort.split(":");
            host = hostAndPortStringArray[0];
            port = Integer.parseInt(hostAndPortStringArray[1]);
        }

        public String getHost() {
            return host;
        }

        public int getPort() {
            return port;
        }
    }
}
