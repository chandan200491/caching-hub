package com.jio.cache.controller;

import com.jio.cache.dto.CacheClearDTO;
import com.jio.cache.dto.CacheInputDTO;
import com.jio.cache.dto.CacheResponseDTO;
import com.jio.cache.dto.StatusBean;
import com.jio.cache.exceptions.BaseApplicationException;
import com.jio.cache.services.CacheService;
import com.jio.cache.services.IJSONService;
import com.jio.cache.services.JSONServiceImpl;
import com.jio.cache.util.CacheClearInputConstant;
import com.jio.cache.util.CacheConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Chandan Singh Karki
 */
@RestController
public class CacheController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private CacheService cacheService;

    /**
     * @param cacheInputDTO
     * @return
     */
    @RequestMapping(value = "/fetchdata", method = RequestMethod.POST)
    public @ResponseBody
    StatusBean getCachedData(@RequestBody CacheInputDTO cacheInputDTO) {
        LOGGER.info("Request Initiated for fetching Master Data: {}", cacheInputDTO);
        StatusBean statusBean = null;
        try {
            CacheResponseDTO cacheResponseDTO = cacheService.getMasterData(cacheInputDTO);
            statusBean = new StatusBean(CacheConstant.SUCCESS_CODE, "Data fetched successfully");
            statusBean.setResult(cacheResponseDTO);
        } catch (BaseApplicationException e) {
            statusBean = new StatusBean(CacheConstant.ERROR_CODE, e.getStatusBean().getStatusMessage());
        } catch (Exception e) {
            LOGGER.error("Exception while fetching Cached data for input parameter :{}", cacheInputDTO, e);
            statusBean = new StatusBean(CacheConstant.ERROR_CODE, "Something went wrong...kindly wait for sometime");
        }
        return statusBean;
    }

    /**
     * @param cacheClearDTO
     * @return
     */
    @RequestMapping(value = "/clear/cache/data", method = RequestMethod.POST)
    public @ResponseBody
    StatusBean removeCachedData(@RequestBody CacheClearDTO cacheClearDTO) {
        LOGGER.info("Request Initiated for Clearing cached data with input parameter:{}", cacheClearDTO);
        StatusBean statusBean = null;
        try {
            cacheService.clearCachedData(cacheClearDTO);
            statusBean = new StatusBean(CacheConstant.SUCCESS_CODE, "Data cleared successfully");
        } catch (BaseApplicationException e) {
            statusBean = new StatusBean(CacheConstant.ERROR_CODE, e.getStatusBean().getStatusMessage());
        } catch (Exception e) {
            LOGGER.error("Exception while Clearing Cached data for input paramter :{}", cacheClearDTO, e);
            statusBean = new StatusBean(CacheConstant.ERROR_CODE, "Something went wrong...kindly wait for sometime");
        }
        return statusBean;
    }

}
