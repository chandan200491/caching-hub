package com.jio.cache.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "MASTER_DATA")
public class MasterDataBean implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "APP_CODE")
    private String appCode;
    @Id
    @Column(name = "OPERATION_CODE")
    private String operationCode;
    @Id
    @Column(name = "LABEL")
    private String label;
    @Id
    @Column(name = "MASTER_KEY")
    private String key;

    @Column(name = "MASTER_VALUE")
    private String value;


    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "UPDATED_DATE")
    private Date updatedDate;

    @Column(name = "IS_ACTIVE")
    private boolean isActive;

    public MasterDataBean() {
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("appCode", appCode)
                .append("operationCode", operationCode)
                .append("label", label)
                .append("key", key)
                .append("value", value)
                .append("createdDate", createdDate)
                .append("updatedDate", updatedDate)
                .append("isActive", isActive)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MasterDataBean that = (MasterDataBean) o;
        return isActive == that.isActive &&
                Objects.equals(appCode, that.appCode) &&
                Objects.equals(operationCode, that.operationCode) &&
                Objects.equals(label, that.label) &&
                Objects.equals(key, that.key) &&
                Objects.equals(value, that.value) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(updatedDate, that.updatedDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(appCode, operationCode, label, key, value, createdDate, updatedDate, isActive);
    }
}
