package com.jio.cache.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "APPLICATION_CODE_MASTER")
public class ApplicationCodeMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "APP_CODE")
    private String appCode;
    @Column(name = "APP_NAME")
    private String appName;

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public ApplicationCodeMaster() {
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("appCode", appCode)
                .append("appName", appName)
                .toString();
    }
}
