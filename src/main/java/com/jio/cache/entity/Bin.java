package com.jio.cache.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "BIN_MASTER")
public class Bin implements Serializable {
    @Id
    @Column(name = "BIN_RANGE_LOW")
    private Long binRangeLow;

    @Id
    @Column(name = "BIN_RANGE_HIGH")
    private Long binRangeHigh;

    @Column(name = "ACCT_FUND_SRC") //credit -C Debit-D Prepaid-P
    private String acctFundSrc;

    @Column(name = "CARD_SCHEME")  //04-Visa,05-Master,03-Amex,99-Rupay
    private String cardScheme;


    @Column(name = "PAN_LENGTH")
    private String panLength;

    public Bin() {
    }

    public String getAcctFundSrc() {
        return acctFundSrc;
    }

    public void setAcctFundSrc(String acctFundSrc) {
        this.acctFundSrc = acctFundSrc;
    }

    public String getCardScheme() {
        return cardScheme;
    }

    public void setCardScheme(String cardScheme) {
        this.cardScheme = cardScheme;
    }

    public Long getBinRangeLow() {
        return binRangeLow;
    }

    public void setBinRangeLow(Long binRangeLow) {
        this.binRangeLow = binRangeLow;
    }

    public Long getBinRangeHigh() {
        return binRangeHigh;
    }

    public void setBinRangeHigh(Long binRangeHigh) {
        this.binRangeHigh = binRangeHigh;
    }

    public String getPanLength() {
        return panLength;
    }

    public void setPanLength(String panLength) {
        this.panLength = panLength;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("acctFundSrc", acctFundSrc)
                .append("cardScheme", cardScheme)
                .append("binRangeLow", binRangeLow)
                .append("binRangeHigh", binRangeHigh)
                .append("panLength", panLength)
                .toString();
    }
}
