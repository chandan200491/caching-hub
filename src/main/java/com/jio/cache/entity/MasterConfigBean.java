package com.jio.cache.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Chandan Singh Karki
 */
@Entity
@Table(name = "CONFIG_MASTER")
public class MasterConfigBean implements Serializable {

    private static final long serialVersionUID = 4631784356420096094L;

    @Id
    @Column(name = "CONFIG_KEY")
    private String key;

    @Column(name = "CONFIG_VALUE")
    private String value;

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("key", key)
                .append("value", value)
                .toString();
    }
}
