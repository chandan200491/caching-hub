package com.jio.cache.entity;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "OPERATION_CODE_MASTER")
public class OperationCodeMaster implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "OPERATION_CODE")
    private String operationCode;
    @Column(name = "OPERATION_NAME")
    private String operationName;

    public OperationCodeMaster() {
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("operationCode", operationCode)
                .append("operationName", operationName)
                .toString();
    }
}
