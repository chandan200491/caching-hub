package com.jio.cache.exceptions;

public class BusinessException extends BaseApplicationException {
    public BusinessException(String statusCode, String statusMessage, Throwable t) {
        super(statusCode, statusMessage, t);
    }

    public BusinessException(String statusCode, String statusMessage) {
        super(statusCode, statusMessage);
    }
}
