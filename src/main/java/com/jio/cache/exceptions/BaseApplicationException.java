package com.jio.cache.exceptions;

import com.jio.cache.dto.StatusBean;

/**
 * @author Chandan Singh Karki
 */

public class BaseApplicationException extends Exception {
    private static final long serialVersionUID = 1L;

    private StatusBean statusBean;

    public BaseApplicationException(String statusCode, String statusMessage, Throwable t) {
        super(statusMessage, t);
        this.statusBean = new StatusBean(statusCode, statusMessage);
    }


    public BaseApplicationException(String statusCode, String statusMessage) {
        super(statusMessage);
        this.statusBean = new StatusBean(statusCode, statusMessage);
    }

    public StatusBean getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(StatusBean statusBean) {
        this.statusBean = statusBean;
    }
}
