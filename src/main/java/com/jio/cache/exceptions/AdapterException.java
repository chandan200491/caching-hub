package com.jio.cache.exceptions;

public class AdapterException extends BaseApplicationException {
    public AdapterException(String statusCode, String statusMessage, Throwable t) {
        super(statusCode, statusMessage, t);
    }

    public AdapterException(String statusCode, String statusMessage) {
        super(statusCode, statusMessage);
    }
}
