package com.jio.cache.exceptions;

/**
 * @author Chandan Singh karki
 */

public class DaoException extends BaseApplicationException {
    private static final long serialVersionUID = 1L;
    public DaoException(String statusCode, String statusMessage) {
        super(statusCode, statusMessage);
    }

    public DaoException(String statusCode, String statusMessage, Throwable t) {
        super(statusCode, statusMessage, t);
    }
}
