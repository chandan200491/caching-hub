package com.jio.cache.dao;

import com.jio.cache.entity.Bin;
import com.jio.cache.entity.MasterConfigBean;
import com.jio.cache.entity.MasterDataBean;
import com.jio.cache.exceptions.DaoException;
import com.jio.cache.util.CacheConstant;
import com.jio.cache.util.InputConstant;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author Chandan Singh Karki
 */
@Component
@Repository
@Transactional
public class MasterDaoImpl implements MasterDAO {
    @Autowired
    private SessionFactory sessionFactory;

    private final Logger LOGGER = LoggerFactory.getLogger(MasterDaoImpl.class);

    /**
     * @param key
     * @return
     * @throws DaoException
     */
    public MasterConfigBean getMasterConfig(String key) throws DaoException {
        try {
            LOGGER.info("fetching value from database for key {}", key);
            Session session = sessionFactory.getCurrentSession();
            @SuppressWarnings("unchecked")
            List<MasterConfigBean> keyValueList = session.createCriteria(MasterConfigBean.class)
                    .add(Restrictions.eq("key", key).ignoreCase()).list();
            if (keyValueList != null && keyValueList.size() > 0) {
                return keyValueList.get(0);
            }
        } catch (Exception e) {
            LOGGER.error("exception while fetching value from database for key {} and cause is {}", key, e);
            throw new DaoException(CacheConstant.DAO_ERROR, CacheConstant.DAO_MESSAGE, e);
        }
        return null;
    }

    /**
     * @return
     * @throws DaoException
     */
    public List<MasterConfigBean> getAllMasterConfigParameter() throws DaoException {
        try {
            LOGGER.info("fetching value from database for all master config parameter");
            List<MasterConfigBean> masterConfigBeans = null;
            Session session = sessionFactory.getCurrentSession();
            masterConfigBeans = session.createQuery("from MasterConfigBean").list();
            return masterConfigBeans;
        } catch (Exception e) {
            LOGGER.error("exception while fetching value from database and cause is {}", e);
        }
        return null;
    }

    /**
     * @param inputMap
     * @return
     * @throws DaoException
     */
    public List<MasterDataBean> getMasterDataFromDb(Map<InputConstant, String> inputMap) throws DaoException {
        try {
            LOGGER.info("fetching value from database for all master data parameter: {}", inputMap);
            Session session = sessionFactory.getCurrentSession();
            Criteria criteria = session.createCriteria(MasterDataBean.class);
            for (InputConstant inputConstant : inputMap.keySet()) {
                switch (inputConstant) {
                    case APP_CODE:
                        criteria.add(Restrictions.eq("appCode", inputMap.get(inputConstant)));
                        break;
                    case OPERATION_CODE:
                        criteria.add(Restrictions.eq("operationCode", inputMap.get(inputConstant)));
                        break;
                    case LABEL:
                        criteria.add(Restrictions.eq("label", inputMap.get(inputConstant)));
                        break;
                    case KEY:
                        criteria.add(Restrictions.eq("key", inputMap.get(inputConstant)));
                        break;

                }
            }
            criteria.addOrder(Order.asc("key"));
            return criteria.list();
        } catch (Exception e) {
            LOGGER.error("exception while fetching data from database for values and cause is {}", inputMap, e);
        }
        return null;
    }

    @Override
    public List<Bin> getBinFromDb(String cardNumber, String panLength) throws DaoException {
        try {
            LOGGER.info("fetching value from database for all bin record for card number: {} and pan length: {}", new Object[]{cardNumber, panLength});
            Session session = sessionFactory.getCurrentSession();
            Criteria criteria = session.createCriteria(Bin.class);
            criteria.add(Restrictions.le("binRangeLow", Long.valueOf(cardNumber)));
            criteria.add(Restrictions.ge("binRangeHigh", Long.valueOf(cardNumber)));
            criteria.add(Restrictions.eq("panLength", panLength));
            return criteria.list();
        } catch (Exception e) {
            LOGGER.error("exception while fetching data from database for card Number: {} and panLength: {} and cause is {}", cardNumber, panLength, e);
        }
        return null;
    }
}
