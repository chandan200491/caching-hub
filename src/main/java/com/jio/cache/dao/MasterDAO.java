package com.jio.cache.dao;

import com.jio.cache.entity.Bin;
import com.jio.cache.entity.MasterConfigBean;
import com.jio.cache.entity.MasterDataBean;
import com.jio.cache.exceptions.DaoException;
import com.jio.cache.util.InputConstant;

import java.util.List;
import java.util.Map;

/**
 * @author Chandan Singh Karki
 */

public interface MasterDAO {
    public MasterConfigBean getMasterConfig(String key) throws DaoException;
    public List<MasterConfigBean> getAllMasterConfigParameter() throws DaoException;
    public List<MasterDataBean> getMasterDataFromDb(Map<InputConstant,String> inputMap) throws DaoException;
    public List<Bin> getBinFromDb(String cardNumber,String panLength) throws DaoException;

}
