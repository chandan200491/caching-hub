package com.jio.cache.adapter;

import com.jio.cache.dto.CacheResponseDTO;
import com.jio.cache.entity.Bin;
import com.jio.cache.entity.MasterDataBean;
import com.jio.cache.exceptions.AdapterException;
import com.jio.cache.services.MasterServiceImpl;
import com.jio.cache.util.CacheConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Component
public class CacheDataAdapter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheDataAdapter.class);

    /**
     * @param masterDataBeans
     * @return List<CacheResponseDTO>
     */
    public CacheResponseDTO tranformMasterData(List<MasterDataBean> masterDataBeans) throws AdapterException {
        try {
            CacheResponseDTO cacheResponseDTO = new CacheResponseDTO();
            Map<String, Map<String, List<Map<String, String>>>> operationCodeMap = new LinkedHashMap<>();
            Map<String, List<Map<String, String>>> dataValueMap = null;
            List<Map<String, String>> datavalueList = null;
            Map<String, String> dataMap = null;
            for (MasterDataBean masterDataBean : masterDataBeans) {
                if (!operationCodeMap.containsKey(masterDataBean.getOperationCode())) {
                    operationCodeMap.put(masterDataBean.getOperationCode(), new HashMap<>());
                }
                dataValueMap = operationCodeMap.get(masterDataBean.getOperationCode());
                if (!dataValueMap.containsKey(masterDataBean.getKey())) {
                    dataValueMap.put(masterDataBean.getKey(), new ArrayList<>());
                }
                datavalueList = dataValueMap.get(masterDataBean.getKey());
                if (!CollectionUtils.isEmpty(datavalueList)) {
                    dataMap = datavalueList.get(0);
                    dataMap.put(masterDataBean.getLabel(), masterDataBean.getValue());
                } else {
                    dataMap = new HashMap<>();
                    dataMap.put(masterDataBean.getLabel(), masterDataBean.getValue());
                    datavalueList.add(dataMap);
                }
            }
            cacheResponseDTO.setOutput(operationCodeMap);
            return cacheResponseDTO;
        } catch (Exception e) {
            LOGGER.error("Exception while transforming master data: {}", e);
            throw new AdapterException(CacheConstant.ERROR_CODE, "Exception while transforming master data");
        }
    }

    /**
     * @param bins
     * @return
     * @throws AdapterException
     */
    public CacheResponseDTO tranformBinData(List<Bin> bins, String cardNumber) throws AdapterException {
        try {
            CacheResponseDTO cacheResponseDTO = new CacheResponseDTO();
            Map<String, Map<String, List<Map<String, String>>>> operationCodeMap = new LinkedHashMap<>();
            Map<String, List<Map<String, String>>> dataValueMap = null;
            List<Map<String, String>> binList = null;
            Map<String, String> dataMap = null;
            for (Bin bin : bins) {
                if (!operationCodeMap.containsKey(CacheConstant.BIN_OPERATION)) {
                    operationCodeMap.put(CacheConstant.BIN_OPERATION, new HashMap<>());
                }
                dataValueMap = operationCodeMap.get(CacheConstant.BIN_OPERATION);
                if (!dataValueMap.containsKey(cardNumber)) {
                    dataValueMap.put(cardNumber, new ArrayList<>());
                }
                binList = dataValueMap.get(cardNumber);
                dataMap = new HashMap<>();
                dataMap.put(CacheConstant.CARD_SCHEME, bin.getCardScheme());
                dataMap.put(CacheConstant.ACCOUNT_SRC, bin.getAcctFundSrc());
                binList.add(dataMap);
            }
            cacheResponseDTO.setOutput(operationCodeMap);
            return cacheResponseDTO;
        } catch (Exception e) {
            LOGGER.error("Exception while transforming Bin Master data: {}", e);
            throw new AdapterException(CacheConstant.ERROR_CODE, "Exception while Bin master data");
        }
    }

}
