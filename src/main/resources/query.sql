CREATE TABLE CONFIG_MASTER (
CONFIG_KEY VARCHAR(255) PRIMARY KEY,
CONFIG_VALUE VARCHAR(1000)
);

CREATE TABLE APPLICATION_CODE_MASTER (
APP_CODE VARCHAR(255) PRIMARY KEY,
APP_NAME VARCHAR(1000)
);

CREATE TABLE OPERATION_CODE_MASTER (
OPERATION_CODE VARCHAR(255) PRIMARY KEY,
OPERATION_NAME VARCHAR(1000)
);

CREATE table MASTER_DATA (
APP_CODE VARCHAR(255),
OPERATION_CODE VARCHAR (255),
LABEL VARCHAR (1000),
MASTER_KEY VARCHAR (500),
MASTER_VALUE VARCHAR (1000),
CREATED_DATE DATE ,
UPDATED_DATE DATE ,
IS_ACTIVE CHAR(1),
PRIMARY KEY(APP_CODE,OPERATION_CODE,LABEL,MASTER_KEY)
);

INSERT INTO CONFIG_MASTER VALUE ('redis.ip','localhost');
INSERT INTO CONFIG_MASTER VALUE ('redis.port','6379');
INSERT INTO CONFIG_MASTER VALUE ('redis.pool.max.total','20');
INSERT INTO CONFIG_MASTER VALUE ('redis.pool.max.idle','20');
INSERT INTO CONFIG_MASTER VALUE ('redis.pool.min.idle','20');

CREATE table BIN_MASTER (
ACCT_FUND_SRC VARCHAR(300),
CARD_SCHEME VARCHAR(300),
BIN_RANGE_LOW CHAR (20),
BIN_RANGE_HIGH CHAR (20),
PAN_LENGTH CHAR(20),
PRIMARY KEY(BIN_RANGE_LOW,BIN_RANGE_HIGH)
);


